package main.java;

/**
 * Created by pjuchnie on 11/25/2016.
 */
public class Address {
    private long id;
    private String street;
    private String buildingNumber;
    private String flatNumber;
    private String postalCode;
    private String city;
    private String country;
}
