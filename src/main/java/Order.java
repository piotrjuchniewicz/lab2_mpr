package main.java;

/**
 * Created by pjuchnie on 11/25/2016.
 */
public class Order {
    private long id;
    private ClientDetails client;
    private Address deliveryAddess;
    private OrderItem[] items;

    public Order()
    {

    }

    public Order(ClientDetails client, Address deliveryAddess, OrderItem[] items)
    {
        super();
        this.client = client;
        this.deliveryAddess = deliveryAddess;
        this.items =  items;
    }
}
